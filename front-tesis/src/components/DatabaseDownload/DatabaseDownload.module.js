import { makeStyles } from '@material-ui/core/styles';


export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    info:{
        backgroundColor:'#d9d9d9',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
    },


}));