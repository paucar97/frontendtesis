import React , {Fragment}from 'react';
import { useStyles } from "./DatabaseDownload.module";
import { Button } from "@material-ui/core";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {Typography} from '@material-ui/core';
import TableHead from '@material-ui/core/TableHead';
import GetAppIcon from '@material-ui/icons/GetApp';
import IconButton from '@material-ui/core/IconButton';

const DatabaseDownload = () => {
  const nameActualMenu = {name: "Database Download"};
  const classes = useStyles();
  const listaVersionesDB = [
    {version:2.2 ,Release_date: '02/14/19',Notes: '66,000 new interactions'},
    {version:2.1 ,Release_date: '07/18/17',Notes: '52,000 new interactions'},
    {version:2.0 ,Release_date: '02/14/16',Notes: '44,000 new interactions, external database identifiers updated'},
    {version:1.8 ,Release_date: '09/01/15',Notes: '45,000 new interactions'},
  ]
  localStorage.setItem('nameActualMenu', JSON.stringify(nameActualMenu));
  return (
    <Fragment>
      <div className = {classes.info}>
        In this section you can download the database used by hippie
      </div>
      <div className = {classes.info}>
        <TableContainer component = {Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell> Version</TableCell>
                <TableCell> Release date</TableCell>
                <TableCell> File (TAB)</TableCell>
                <TableCell> PSI-MI</TableCell>
                <TableCell> Notes</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {listaVersionesDB.map((row) => (
                <TableRow key= {row.version}>
                  <TableCell component= "th" scope = "row">
                    {row.version}
                  </TableCell>
                  <TableCell>{row.Release_date} </TableCell>
                  <TableCell > 
                    <IconButton>
                      <GetAppIcon fontSize="large"/>
                    </IconButton>
                  </TableCell>
                  <TableCell > 
                    <IconButton>
                      <GetAppIcon fontSize="large"/>
                    </IconButton>
                  </TableCell>
                  <TableCell>{row.Notes} </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </Fragment>
      
  );
};

export default DatabaseDownload;