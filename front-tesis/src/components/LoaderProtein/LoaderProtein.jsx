import React,{Fragment,useContext} from 'react';
import { useStyles } from "./LoaderProtein.module";
import { Button ,CircularProgress} from "@material-ui/core";
import { MainContext } from '../../context/mainContext';

const LoaderProtein = () =>{
    const classes = useStyles();
    return (
        <Fragment>
            <div className={classes.loader} >
                <CircularProgress size={100}/>
            </div>
        </Fragment>
    );
}

export default LoaderProtein;