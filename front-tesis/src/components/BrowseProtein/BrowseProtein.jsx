import React, { Fragment ,useContext,useEffect} from 'react';
import MaterialTable from "material-table";
import { Button, CircularProgress } from "@material-ui/core";
import { useStyles } from "./BrowseProtein.module";
import { ProteinList } from '../../assets/data/ProteinList'
import { Link } from 'react-router-dom';
import {routes} from '../../routes';
import { MainContext } from '../../context/mainContext';
import {getProteins} from '../../api/index';
const BrowseProtein = (props) => {
  const context=useContext(MainContext);
  const [dataProteinList,setdataProteinList] = React.useState([]);
  const [dataListReal,setdataListReal] = React.useState({});
  const [dataFetched, setDataFetched] = React.useState(false);
  
  let dataAux = [];
  let s;
  useEffect   ( ()=> {
    //s = functionTest();
  } , [context.entrez_id,context.uniprot_id,context.gene_symbol,context.proteinIdentifiers,context.menuInformation]);

  
  const classes = useStyles();
  let data = ProteinList;
  const nameActualMenu = {name: "Browse Protein"};
  localStorage.setItem('nameActualMenu', JSON.stringify(nameActualMenu));

  const menuAux = {...context.menuInformation};
  menuAux.header = 'Browse Protein';  
  

  const optionsTable={
      toolbar:true,
      filtering:true,
      draggable: false,
      showTitle: false,
      
      pageSize: 10,
      actionsColumnIndex: -1,
      headerStyle: {
        color: "#004178",
      },
  };

  const columnsData=[
    { title: "Uniprot ID", field: "uniprotId" },
    { title: "Entrex id", field: "entrezId" },
    { title: "Gene Symbol", field: "geneSymbol" },
    { title: "Degree", field: "degree" },
    { title: "Average score", field: "averageScore" },
    { title: "Id",field : "idProtein"}
  ]
  
  const functionTest = async () =>{
    console.log("HOLA");
    const dataApi = await getProteins();
    console.log(dataApi);
    let dataList = new Array();
    await dataApi.data.forEach((elem) =>{
      
      dataList.push(
        {
          idProtein : elem.id_protein,
          uniprotId : elem.sequences_info[0].id_uniprot,
          geneSymbol : elem.sequences_info[0].gene_symbol,
          entrezId : elem.entrezes[0] ? elem.entrezes[0].entrez_id : "",
          degree : 0,
          averageScore : 0
        })
    })
    //console.log(dataList.length);
    dataAux = {...dataProteinList};
    dataAux = JSON.stringify(dataList)
    await setdataProteinList(dataAux);
    //console.log("sdads",dataList);
    await setdataListReal({data:dataList});
    console.log(dataListReal);
    if (!dataListReal)
      return functionTest();
    return {data:dataList}
  };

  const setIdentifiersProtein = async (event,rowData)=>{
    let proteinIdentifiers_aux = {...context.proteinIdentifiers};
    console.log("rowData.id_entrez",rowData)
    proteinIdentifiers_aux.id   = rowData.idProtein;
    proteinIdentifiers_aux.id_entrez   = rowData.entrezId;
    proteinIdentifiers_aux.id_uniprot  = rowData.uniprotId;
    proteinIdentifiers_aux.gene_symbol = rowData.geneSymbol;
    
    await context.setProteinIdentifiers(proteinIdentifiers_aux);
    await localStorage.setItem('proteinIdentify', JSON.stringify(proteinIdentifiers_aux));
    props.history.push({pathname:"/home/proteinInteraction"});
    console.log(context.proteinIdentifiers);
    
  }

  return (
      <Fragment className={classes.root}>
        <div className={classes.info}>
            In this section you can search protein in HIPPIE's database
        </div>

        { !dataListReal.data?
        <MaterialTable
        style={{ width: "100%" }}
        columns={columnsData}
        data={functionTest}
        localization={{
          header: { actions: "Interactions proteins" },
        }}
        actions={[
          (rowData) => ({
            icon: () => (<Button className={classes.button}  >Show</Button>),
            onClick: (event, rowData) => setIdentifiersProtein(event, rowData)
          })
        ]}
        options={optionsTable}
      />
        :
        <MaterialTable
          style={{ width: "100%" }}
          columns={columnsData}
          data={dataListReal.data}
          localization={{
            header: { actions: "Interactions proteins" },
          }}
          actions={[
            (rowData) => ({
              icon: () => (<Button className={classes.button}  >Show</Button>),
              onClick: (event, rowData) => setIdentifiersProtein(event, rowData)
            })
          ]}
          options={optionsTable}
        />
        }
      </Fragment>
  );
};

export default BrowseProtein;