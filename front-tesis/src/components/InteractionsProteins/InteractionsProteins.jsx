import React,{Fragment ,useContext,useEffect,useRef,useState} from 'react';
import { useStyles } from "./InteractionsProteins.module";
import IdentifierTable from "../IdentifierTable/IdentifierTable" 
import { MainContext } from '../../context/mainContext';
import { Button ,CircularProgress} from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import MaterialTable from "material-table";
import {InteractionProteinList} from '../../assets/data/InteractionProteinList';
import {getInteractionsOneProteins} from '../../api/index';
const InteractionsProteins = (props) => {
    const textAreaRef = useRef(null);
    const classes = useStyles();
    const context=useContext(MainContext);
    const data = InteractionProteinList;
    const [testingCode,setTestingCode] = useState("");
    const [dataProteinList,setdataProteinList] = React.useState([]);
    const [dataListReal,setdataListReal] = React.useState({});
    const [dataFetched, setDataFetched] = React.useState(false);
    let proteinIdentifiers;
    let auxDataFetch;
    let dataAux = [];
    useEffect(() => {
        
        if (context.proteinIdentifiers){
            
            localStorage.setItem('proteinIdentify', JSON.stringify(context.proteinIdentifiers));
             proteinIdentifiers = context.proteinIdentifiers;
             functionTest(context.proteinIdentifiers.id)
        }else{
            
             proteinIdentifiers =  JSON.parse(localStorage.getItem('proteinIdentify'));
             functionTest(proteinIdentifiers.id)
        }
        console.log("proteinIdentifiers - use effect",proteinIdentifiers.id)
        
    }, [context.proteinIdentifiers,context.listaInteractions]);

    proteinIdentifiers = JSON.parse(localStorage.getItem('proteinIdentify'));
    const columnsData=[
        { title: "Id",field : "idProtein"},
        { title: "Uniprot ID", field: "uniprotId" },
        { title: "Entrez id", field: "entrezId" },
        { title: "Gene Symbol", field: "geneSymbol" },
        { title: "Score", field: "score" },
      ]

    const optionsTable={
        toolbar:true,
        filtering:true,
        draggable: false,
        showTitle: false,
        pageSize: 5,
        actionsColumnIndex: -1,
        headerStyle: {
        color: "#004178",
      },
    };
    const copyTestingCode = (event) => {
        const elem = document.createElement('textarea');
        let text = ""
        // data es lo que devuelve el api
        dataListReal.data.map((eleme) =>{
            text = text + eleme.uniprotId + "\t" + eleme.entrezId +"\t"+ eleme.geneSymbol+"\n";
        });
        elem.value = text;
        document.body.appendChild(elem);
        elem.select();
        document.execCommand('copy');
        document.body.removeChild(elem);
    };
    const setIdentifiersProtein = async (event,rowData)=>{
        
        setdataListReal(null);
        console.log("dataListReal - setIdentifiersProtein",dataListReal);
        auxDataFetch = {...dataFetched};
        auxDataFetch = false
        setDataFetched(auxDataFetch);
        
        //functionTest(0)
        console.log("functionTest - setIdentifiersProtein",rowData.idProtein);
        let proteinIdentifiers_aux = {...context.proteinIdentifiers};
        proteinIdentifiers_aux.id = rowData.idProtein;
        proteinIdentifiers_aux.id_entrez   = rowData.entrezId;
        proteinIdentifiers_aux.id_uniprot  = rowData.uniprotId;
        proteinIdentifiers_aux.gene_symbol = rowData.geneSymbol;
        
        await context.setProteinIdentifiers(proteinIdentifiers_aux);
        await localStorage.setItem('proteinIdentify', JSON.stringify(proteinIdentifiers_aux));
        //console.log("martin",props)
        props.history.push({pathname:"/home/proteinInteraction"});
        

        
    }
    const functionTest = async (idProteinF) =>{
        setDataFetched(false);

        console.log('functionTest', idProteinF);
        //proteinIdentifiers =  JSON.parse(localStorage.getItem('proteinIdentify'));
        //idProteinF = context.proteinIdentifiers.id;
        const dataApi = await getInteractionsOneProteins(idProteinF);
        
        let dataList = new Array();
        await dataApi.data.forEach((elem) =>{
          
          dataList.push(
            {
              idProtein : elem.id_protein2,
              uniprotId : elem.protein2.sequences_info[0].id_uniprot,
              geneSymbol : elem.protein2.sequences_info[0].gene_symbol,
              entrezId : elem.protein2.entrezes[0] ? elem.protein2.entrezes[0].entrez_id : "",
              degree : 0,
              averageScore : 0
            })
        })
        //dataAux = {...dataProteinList};
        //dataAux = JSON.stringify(dataList)
        //await setdataProteinList(dataAux);
        //console.log("dataList",dataList);

        await setdataListReal({data:dataList})
        setDataFetched(true);
        
        
        return {data:dataList}
    };

    const abrirInteractionsNetwork = async (proteinIdentifierObj, ListRealObj) => {
        console.log("abrirInteractionsNetwork",proteinIdentifierObj,ListRealObj);
        await context.setDataListInteractions(ListRealObj);
        await localStorage.setItem('dataListaReal', JSON.stringify(ListRealObj));
        props.history.push({pathname:"/home/InteractionsNetwork"});
    };
    return (
        <Fragment>
            
            <Grid container style={{display:'flex'}}>
                <Grid item xs={10}>
                    <IdentifierTable proteinIdentifierObj = {proteinIdentifiers}></IdentifierTable>
                </Grid>
                <Grid item xs={2} >
                    <Button className={classes.button} onClick={() => (abrirInteractionsNetwork(proteinIdentifiers,dataListReal.data))}>Visualize <br/> Network</Button>
                    <Button className={classes.button} onClick={copyTestingCode}>Copy Content</Button>
                </Grid>
                
            </Grid>
            {!dataListReal || !dataFetched ?
                <Fragment>
                    <div style={{display:'flex',alignItems:'center',justifyContent:'center',height:'400px'}}>
                        <CircularProgress />
                    </div>
                </Fragment>
            :
                <MaterialTable 
                columns={columnsData}
                localization={{
                    header: { actions: "Interactions proteins" },
                }}
                options={optionsTable}
                style={{ width: "100%" }}
                data={dataListReal.data}
                actions={[
                (rowData) => ({
                    icon: () => (<Button className={classes.buttonRow}   >Show</Button>),
                    onClick: (event, rowData) => setIdentifiersProtein(event, rowData)
                    })
                ]}
                />
            }
            
        </Fragment>
    );
};

export default InteractionsProteins;