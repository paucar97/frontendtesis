import React,{Fragment ,useContext,useEffect,useRef,useState} from 'react';
import { useStyles } from "./InteractionsNetworkPageIndirect.module";
import { MainContext } from '../../context/mainContext';
import { CircularProgress,Typography} from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import { Graph } from "react-d3-graph";
import {getSequenceInfo} from '../../api/index';
import TextField from '@material-ui/core/TextField';

const InteractionsNetworkPageIndirect = (props) =>{
    const classes = useStyles();
    const context=useContext(MainContext);
    const {proteinIdentifierObj, dataListInteractions} = props;
    
    let dataGraphProteinsInteractionsIndirect;
    const [dataGraph,setDataGraph] = useState({});
    const [dataProteinInfo,setDataProteinInfo] = useState({});
    const [dataLinkInfo,setDataLinkinfo] = useState({});
    const [dataFecth, setDataFecth] = useState(false);
    useEffect(()=>{
        console.log("Out useEffect",context.dataGraphProteinsInteractionsIndirect)
        if (context.dataGraphProteinsInteractionsIndirect){
            
            localStorage.setItem('dataGraphProteinsInteractionsIndirect', JSON.stringify(context.dataGraphProteinsInteractionsIndirect));
            dataGraphProteinsInteractionsIndirect = context.dataGraphProteinsInteractionsIndirect;
            console.log("if useEffect", dataGraphProteinsInteractionsIndirect);
            //setDataGraph(context.dataGraphProteinsInteractionsIndirect);
            handleDataGraph(context.dataGraphProteinsInteractionsIndirect);
        
        }else{
            console.log("useEffect1",dataGraph);
            dataGraphProteinsInteractionsIndirect =  JSON.parse(localStorage.getItem('dataGraphProteinsInteractionsIndirect'));
            //localStorage.setItem('dataGraphProteinsInteractionsIndirect', JSON.stringify(context.dataGraphProteinsInteractionsIndirect));
            console.log("useEffect2",dataGraph);
            handleDataGraph(dataGraphProteinsInteractionsIndirect);
            
        }
        console.log("Effect",dataGraph);
        //setDataGraph(context.dataGraphProteinsInteractionsIndirect);
    },[context.dataGraphProteinsInteractionsIndirect]);
    
    //dataGraphProteinsInteractionsIndirect = JSON.parse(localStorage.getItem('dataGraphProteinsInteractionsIndirect'));

    const onClickNode = async (nodeId, node)=> {
        //window.alert(`Clicked node ${nodeId}`);
        await setDataProteinInfo({});
        setDataFecth(true);
        let dataProteinInfoAux = {};
        
        dataProteinInfoAux.geneSymbolInfo = node.gene_symbol;
        dataProteinInfoAux.uniprotIdCardInfo = node.id_uniprot;

        dataProteinInfoAux.entrezIdCardInfo = node.id_entrez;
        dataProteinInfoAux.databaseSourceInfo = 'Uniprot';
        dataProteinInfoAux.cantPPIInfo = node.cantPPI;
        await setDataProteinInfo(dataProteinInfoAux);
    };
      
    const onClickLink = async (source, target)=> {
        //window.alert(`Clicked link between ${source} and ${target}`);
        let dataLinkInfoAux = {};
        console.log(dataGraph);
        await dataGraph.nodes.forEach((elem) =>{
            
            if (elem.id == source){
                console.log("elem.id",elem.id);
                dataLinkInfoAux.protein1 = elem.id_entrez;
            }                
            if (elem.id == target){
                dataLinkInfoAux.protein2 = elem.id_entrez;
                console.log("elem.id",elem.id);

            }
        })
        
        dataLinkInfoAux.dbSource = 'Uniprot';
        dataLinkInfoAux.weigth = '0.9';
        setDataLinkinfo(dataLinkInfoAux);
    };

    const handleDataGraph = async (dataAux) =>{
        await setDataGraph(dataAux);
        console.log("handleDataGraph",dataAux);
    }


    const data = {
            nodes: [{ id: "Harry" ,color:"red"}, { id: "Sally" }, { id: "Alice" },{ id: "Alice" }],
            links: [
              { source: "Harry", target: "Sally" },
              { source: "Harry", target: "Alice" },
              { source: "Alice", target: "Harry" },
            ]
    };

    const myConfigGraph = {
        nodeHighlightBehavior: true,
        linkHighlightBehavior: true,
        highlightOpacity: 0.2,
        width : '1000',
        node: {
          color: "#677EF5",
          size: 380,
          highlightStrokeColor: "blue",
          labelProperty : "gene_symbol"
        },
        link: {
          highlightColor: "red",
          strokeWidth : 3
        },
    };
    
    return (
        <Fragment>
            <Grid container style={{display:'flex'}}>
                <Grid item xs={9}>
                    <div  className = {classes.cardInformation}>
                    <Graph
                        
                        id="graph-id-2" // id is mandatory
                        data={dataGraph}
                        config={myConfigGraph}
                        onClickNode={onClickNode}
                        onClickLink={onClickLink}
                    />
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div className = {classes.cardInformation}>
                       <form >
                           <TextField id="search-node" label="Search" size= "small"/>
                       </form>
                       <Typography style={{textAlign:'middle',fontSize:'30px', alignSelf:'center',color:'#00AC8D'}} variant="h6" >Protein </Typography>
                       { !dataProteinInfo.uniprotIdCardInfo && dataFecth ?
                            <div style={{display:'flex',alignItems:'center',justifyContent:'center',height:'40px'}}>
                                <CircularProgress/>
                            </div>
                        :
                            <div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Uniprot</Typography>
                                        <Typography>{dataProteinInfo.uniprotIdCardInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Entrez ID</Typography>
                                        <Typography >{dataProteinInfo.entrezIdCardInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Gene Symbol</Typography>
                                        <Typography >{dataProteinInfo.geneSymbolInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Database Source</Typography>
                                        <Typography >{dataProteinInfo.databaseSourceInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Cant PPI</Typography>
                                        <Typography >{dataProteinInfo.cantPPIInfo}</Typography>
                                    </div>
                            </div>
                       }

                    </div>
                    <div className = {classes.cardInformation}>
                    <Typography style={{textAlign:'middle',fontSize:'30px', alignSelf:'center',color:'#00AC8D'}} variant="h6" >Interaction </Typography>
                    <div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>Protein 1</Typography>
                                <Typography>{dataLinkInfo.protein1}</Typography>
                            </div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>Protein 2</Typography>
                                <Typography >{dataLinkInfo.protein2}</Typography>
                            </div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>DB Source</Typography>
                                <Typography >{dataLinkInfo.dbSource}</Typography>
                            </div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>Weigth</Typography>
                                <Typography >{dataLinkInfo.weigth}</Typography>
                            </div>
                       </div>
                    </div>

                </Grid>
            </Grid>
        </Fragment>

    );
}


export default InteractionsNetworkPageIndirect;