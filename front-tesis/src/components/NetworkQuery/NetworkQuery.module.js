import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) =>({
    info:{
        backgroundColor:'#d9d9d9',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
    },
    headerInfo:{
        backgroundColor:'#409FF5',
        padding:'10px',
        color:'#000',
        borderRadius:'25px',
        fontFamily:'Roboto',
        fontSize:'23px',
        marginBottom:'25px',
        display: 'flex'
    },
    containerQuerySet:{
        backgroundColor:'#F6F6F6',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
        display: 'flex'
    },
    buttonSubmit:{
        padding:'10px 25px',
        backgroundColor:'#59DFC7',
        borderRadius:'25px',
        '&:hover':{
          backgroundColor:'rgb(43, 130, 207)'
        },
        
        alignItems: "center",
        justifyContent: "center",
    },
    divInformation:{
        display:"flex",
        alignItems: "center",
        justifyContent: "center",
    },
    iconInformation:{
        color:"#EDE067"
    },
    iconInformationTwo :{
        color:"#679CED",
        marginLeft: "10px",
        marginTop: "5px"
    }

}));