import React,{Fragment,useContext} from 'react';
import { useStyles } from "./NetworkQuery.module";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { MainContext } from '../../context/mainContext';
import { FormControl } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Dropzone from '../DropzoneFile/Dropzone';
import UploadFiles from "../DropzoneFile/UploadFilesComp";
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Tooltip from '@material-ui/core/Tooltip';
import IdentifierTable from '../IdentifierTable/IdentifierTable';
import {getIteractionsProteins,getIteractionsProteinsIndirect} from '../../api/index';
const NetworkQuery = (props) => {
  const classes = useStyles();
  const nameActualMenu = {name: "Network Query"};
  const context=useContext(MainContext);
  const fileProtein = [];
  const [listProteins, setListProteins] = React.useState(""); // se recibe la lista de input de proteinas
  const [tuplaProteins,setTuplaProteins] = React.useState("");
  const [outputType,setOutputType] = React.useState(1);
  const [numberPPI,setNumberPPI] = React.useState(1);
  const [layer, setLayer] = React.useState(1);
  const [confidenceValue,setConfidenceValue] = React.useState("0.001");
  const [disableConfidenceScore,setDisableConfidenceScore] = React.useState(false);
  const [disableListProteins,setDisableListProteins] = React.useState(false);
  

  const handleDisableListProteins = (event) =>{
    if (disableListProteins)
      setDisableListProteins(false);
    else
      setDisableListProteins(true);
  };

  const handleDisableConfidence = (event) =>{
    if (disableConfidenceScore)
      setDisableConfidenceScore(false);
    else
      setDisableConfidenceScore(true);
    
  }
  const handleChangeOutputType = (event) =>{
    setOutputType(event.target.value);
  }

  const handleChangeIdentifierProtein = (event) =>{
    setListProteins(event.target.value);
    
  };
  const handleTuplaProteins = async (event) =>{
    setTuplaProteins(event.target.value);
  }

  const handleNumberPPI = async (event) => {
    await setNumberPPI(event.target.value);
    //console.log(event.target.value);
  };

  const handleLayer = (event) =>{
    setLayer(event.target.value);
  };
  const handleConfidenceValue = (event) =>{
    setConfidenceValue(event.target.value);
  }

  const handleConfidenceValueBySelect = (event) =>{
    if (event.target.value === 1)
      setConfidenceValue(0);
    if (event.target.value === 2)
      setConfidenceValue(0.63);
    if (event.target.value === 3)
      setConfidenceValue(0.72);      
  }
  const searchInteractionsProteins = async (event) =>{
    //console.log("searchInteractionsProteins",listProteins);
    const aux = listProteins.replace(/\n/g,";");
    console.log("searchInteractionsProteins",aux);
    const dataApi = await getIteractionsProteins(aux,numberPPI);
    console.log(dataApi);
    await context.setDataGraphProteinsInteractionsIndirect(dataApi.data);
    await localStorage.setItem('dataGraphProteinsInteractionsIndirect', JSON.stringify(dataApi.data));
    console.log("searchInteractionsProteins",dataApi);
    props.history.push({pathname:"/home/InteractionsNetworkProteins"});
  };

  const searchIndirectInteractions = async (event) =>{
    const aux = tuplaProteins.replace(/\n/g,";");
    console.log("searchIndirectInteractions",aux);
    const dataApi = await getIteractionsProteinsIndirect(aux,numberPPI);
    console.log(dataApi);
    await context.setDataGraphProteinsInteractionsIndirect(dataApi.data);
    await localStorage.setItem('dataGraphProteinsInteractionsIndirect', JSON.stringify(dataApi.data));
    console.log("searchInteractionsProteins",dataApi);
    props.history.push({pathname:"/home/InteractionsNetworkProteins"});
  }

  localStorage.setItem('nameActualMenu', JSON.stringify(nameActualMenu));
  return (
      <Fragment>
        <div className={classes.info}>
          In this section you can search protein in HIPPIE database
        </div>
        <div style={{"margin-left":"1000px","color":"red","font-size":"17px"}}>
          (*) Obligatory field  
        </div>
        <div className={classes.headerInfo}>
          Query set  
          <div style = {{"margin-left":"0.5%","font-size":"14px","color":"#D70101"}}> (*)</div>
        </div>
        <Grid container  className={classes.containerQuerySet}>
          <Grid item xs={6}>
            <div style = {{'margin-top':"5px"}}>Input list of proteins: </div>
          </Grid>
          <Grid item xs={6}>
            <FormControlLabel
            control={
              <Checkbox
                color = 'primary'
                onChange={handleDisableListProteins}
              />
            }
            label = "File of proteins:"
            />
          </Grid>
          <Grid item xs = {6} style={{'display':'flex'}}>
            <TextField  style={{"white-space": "pre-line"}} disabled={disableListProteins} id="input-proteins-list" label="Input proteins" variant="outlined" multiline= "true" onChange={(e) => handleChangeIdentifierProtein(e)} />
            <div style={{'marginLeft':'20px'}}>Example Input: <br/>
            dnmt3a dnmt3b <br/>
            ywhaz ywhaz
            </div>
          </Grid>
          <Grid item xs ={6}>
            <UploadFiles></UploadFiles>
          </Grid>
          <Grid item xs = {12} >
            <div style ={{"display":"flex","align-items":"center","justify-content":"center"}}>
              <Button className={classes.buttonSubmit} onClick={searchInteractionsProteins}>Search</Button>
            </div>
          </Grid>
          <Grid item xs = {12} style={{'display':'flex',"align-items":"center","justify-content":"center"}}>
            <InfoIcon className ={classes.iconInformation} />
            <div className= {classes.divInformation}>(this may take a while)</div>
          </Grid>
        </Grid>
        <div className={classes.headerInfo}>
          Output parameters
          <div style = {{"margin-left":"0.5%","font-size":"14px","color":"#D70101"}}> (*)</div>
        </div>
        <Grid container  className={classes.containerQuerySet}>
          <Grid item xs ={6} style={{'display':'flex'}}>
              <div>Output type: </div> 
              <InputLabel id="identifierProtein-label" style={{"margin-left":"20px"}}></InputLabel>
              <Select
                labelId="identifierProtein-label"
                id="identifierProtein"
                value={outputType}
                onChange={handleChangeOutputType}
                
              >
                <MenuItem value={1}>Visualization Browser</MenuItem>
                <MenuItem value={2}>Text Browser</MenuItem>
                <MenuItem value={3}>Write file (TAB)</MenuItem>
              </Select>
          </Grid>
          <Grid item xs={6} style={{'display':'flex'}} >
            <div> Min. number of PPIs to query set</div>
            <InputLabel id="identifierProtein-label" style={{"margin-left":"20px"}}></InputLabel>
              <Select
                labelId="identifierProtein-label"
                id="identifierProtein"
                value={numberPPI}
                onChange={handleNumberPPI}
                
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4</MenuItem>
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={6}>6</MenuItem>
                <MenuItem value={7}>7</MenuItem>
              </Select>
              <Tooltip title={"Number of PPI"} placement='right-start'>
                <InfoIcon className ={classes.iconInformationTwo} />
              </Tooltip>
          </Grid>
          <Grid item xs={6} style={{'display':'flex'}}>
            <div>Layers:</div>
            <InputLabel id="identifierProtein-label" style={{"margin-left":"20px"}}></InputLabel>
              <Select
                labelId="identifierProtein-label"
                id="identifierProtein"
                value={layer}
                onChange={handleLayer}
                
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
              </Select>
            <Tooltip title={"Layer of PPi"} placement='right-start'>
              <InfoIcon className ={classes.iconInformationTwo} />
            </Tooltip>
          </Grid>
        
        </Grid>
        <div className={classes.headerInfo}>
          Score filter
        </div>
        <Grid container  className={classes.containerQuerySet}>
          <Grid item xs={12} style = {{'display':'flex'}}>
            <div>Confidence score:</div>
            <TextField disabled={disableConfidenceScore} style={{'marginLeft':'10px','width':'50px'}}  size = 'small' onChange={handleConfidenceValue} value={confidenceValue}></TextField>

          </Grid>
          <Grid item xs={12} style = {{'display':'flex'}}>
          <FormControlLabel
              control={
                <Checkbox
                  color = 'primary'
                  onChange= {handleDisableConfidence}
                />
              }
              label = "predefined confidence level"
              />
              <Select
                labelId="identifierProtein-label"
                id="identifierProtein"
                
                onChange={handleConfidenceValueBySelect}
                disabled = {!disableConfidenceScore}
              >
                <MenuItem value={1}>no filter</MenuItem>
                <MenuItem value={2}>medium confidence (0.63)</MenuItem>
                <MenuItem value={3}>high confidence (0.72)</MenuItem>
              </Select>
          </Grid>
        </Grid>
        
        <div className= {classes.headerInfo}>
          Indirect Interactions
        </div>
        <Grid container  className={classes.containerQuerySet}>
          <Grid item xs={12}>
              <div style = {{'margin-top':"5px"}}>Input list of proteins: </div>
          </Grid>
          <Grid item xs = {6} style={{'display':'flex'}}>
              <Grid item xs={6} style = {{'display':'flex'}}>
                  <TextField  style={{"white-space": "pre-line"}} disabled={disableListProteins} id="input-proteins-list-indirect" label="Input proteins" variant="outlined" multiline= "true" onChange={(e) => handleTuplaProteins(e)} />
                  <div style={{'marginLeft':'20px'}}>Example Input: <br/>
                  dnmt3a dnmt3b <br/>
                  </div>
              </Grid>
              <Grid item xs = {6} >
                <div style ={{"display":"flex","align-items":"center","justify-content":"center"}}>
                  <Button className={classes.buttonSubmit} onClick={searchIndirectInteractions}>Search</Button>
                </div>
              </Grid>
          </Grid>
        </Grid>
      </Fragment>
      
  );
};

export default NetworkQuery;