import React,{Fragment ,useContext,useEffect,useRef,useState} from 'react';
import { useStyles } from "./InteractionsNetworkPage.module";
import IdentifierTable from "../IdentifierTable/IdentifierTable" 
import { MainContext } from '../../context/mainContext';
import { Button ,CircularProgress,Typography} from "@material-ui/core";
import Grid from '@material-ui/core/Grid';
import MaterialTable from "material-table";
import {InteractionProteinList} from '../../assets/data/InteractionProteinList';
import { Graph } from "react-d3-graph";
import {getSequenceInfo} from '../../api/index';
import TextField from '@material-ui/core/TextField';

const InteractionsNetworkPage = (props) =>{
    const classes = useStyles();
    const context=useContext(MainContext);
    const {proteinIdentifierObj, dataListInteractions} = props;
    
    let proteinIdentifiers;
    const [dataGraph,setDataGraph] = useState({});
    const [dataProteinInfo,setDataProteinInfo] = useState({});
    const [dataLinkInfo,setDataLinkinfo] = useState({});
    const [dataFecth, setDataFecth] = useState(false);
    var uniprotIdCardInfo = '1433E_HUMAN';
    const entrezIdCardInfo = '1433E_HUMAN';
    const geneSymbolInfo = '1433E_HUMAN';
    const databaseSourceInfo = '1433E_HUMAN';
    const cantPPIInfo = '1433E_HUMAN';
    useEffect(()=>{
        if (context.proteinIdentifiers){
            creataFormatDataGraph();
            localStorage.setItem('proteinIdentify', JSON.stringify(context.proteinIdentifiers));
            proteinIdentifiers = context.proteinIdentifiers;
            
        }else{
            creataFormatDataGraph();
            proteinIdentifiers =  JSON.parse(localStorage.getItem('proteinIdentify'));
            
        }
    },[context.proteinIdentifiers])
    
    proteinIdentifiers = JSON.parse(localStorage.getItem('proteinIdentify'));
    const onClickNode = async (nodeId)=> {
        //window.alert(`Clicked node ${nodeId}`);
        await setDataProteinInfo({});
        setDataFecth(true);
        let dataProteinInfoAux = {...dataProteinInfo};
        const dataApi = await getSequenceInfo(nodeId);
        console.log(dataApi.data);
        
        dataProteinInfoAux.geneSymbolInfo = dataApi.data.gene_symbol;
        dataProteinInfoAux.uniprotIdCardInfo = dataApi.data.id_uniprot;

        dataProteinInfoAux.entrezIdCardInfo = nodeId;
        dataProteinInfoAux.databaseSourceInfo = 'Uniprot';
        if (nodeId === proteinIdentifiers.id_entrez)
            dataProteinInfoAux.cantPPIInfo = dataGraph.links.length;
        else
            dataProteinInfoAux.cantPPIInfo = 1;
        await setDataProteinInfo(dataProteinInfoAux);
    };
      
    const onClickLink = async (source, target)=> {
        //window.alert(`Clicked link between ${source} and ${target}`);
        let dataLinkInfoAux = {};
        dataLinkInfoAux.protein1 = source;
        dataLinkInfoAux.protein2 = target;
        dataLinkInfoAux.dbSource = 'Uniprot';
        dataLinkInfoAux.weigth = '0.9';
        setDataLinkinfo(dataLinkInfoAux);
    };
    const data = {
            nodes: [{ id: "Harry" ,color:"red"}, { id: "Sally" }, { id: "Alice" },{ id: "Alice" }],
            links: [
              { source: "Harry", target: "Sally" },
              { source: "Harry", target: "Alice" },
              { source: "Alice", target: "Harry" },
            ]
    };
    const creataFormatDataGraph = async() =>{
        let dataGraphNode = new Array({id:proteinIdentifiers.id_entrez,color:"red"}); // primer nodo
        let dataGraphLinks = new Array();
        let ListRealObj =  JSON.parse(localStorage.getItem('dataListaReal'));
        await ListRealObj.forEach((elem) =>{
            dataGraphNode.push({
                id : elem.entrezId,
            });
            dataGraphLinks.push({
                source:proteinIdentifiers.id_entrez,
                target : elem.entrezId
            });
        })
        const rpta = {nodes: dataGraphNode, links :dataGraphLinks }
        await setDataGraph(rpta);
        console.log("creataFormatDataGraph",rpta);

        return rpta;
    };
    const myConfigGraph = {
        nodeHighlightBehavior: true,
        linkHighlightBehavior: true,
        highlightOpacity: 0.2,
        width : '1000',
        node: {
          color: "#677EF5",
          size: 380,
          highlightStrokeColor: "blue",
        },
        link: {
          highlightColor: "red",
          strokeWidth : 3
        },
      };
    console.log("externo",data);
    return (
        <Fragment>
            <Grid container style={{display:'flex'}}>
                <Grid item xs={12}>
                    <IdentifierTable proteinIdentifierObj = {proteinIdentifiers}></IdentifierTable>
                </Grid>
            </Grid>
            <Grid container style={{display:'flex'}}>
                <Grid item xs={9}>
                    <div  className = {classes.cardInformation}>
                    <Graph
                        
                        id="graph-id-1" // id is mandatory
                        data={dataGraph}
                        config={myConfigGraph}
                        onClickNode={onClickNode}
                        onClickLink={onClickLink}
                    />
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div className = {classes.cardInformation}>
                       <form >
                           <TextField id="search-node" label="Search" size= "small"/>
                       </form>
                       <Typography style={{textAlign:'middle',fontSize:'30px', alignSelf:'center',color:'#00AC8D'}} variant="h6" >Protein </Typography>
                       { !dataProteinInfo.uniprotIdCardInfo && dataFecth ?
                            <div style={{display:'flex',alignItems:'center',justifyContent:'center',height:'40px'}}>
                                <CircularProgress/>
                            </div>
                        :
                            <div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Uniprot</Typography>
                                        <Typography>{dataProteinInfo.uniprotIdCardInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Entrez ID</Typography>
                                        <Typography >{dataProteinInfo.entrezIdCardInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Gene Symbol</Typography>
                                        <Typography >{dataProteinInfo.geneSymbolInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Database Source</Typography>
                                        <Typography >{dataProteinInfo.databaseSourceInfo}</Typography>
                                    </div>
                                    <div style={{display:'flex',justifyContent:'space-between'}}>
                                        <Typography variant='h6'>Cant PPI</Typography>
                                        <Typography >{dataProteinInfo.cantPPIInfo}</Typography>
                                    </div>
                            </div>
                       }

                    </div>
                    <div className = {classes.cardInformation}>
                    <Typography style={{textAlign:'middle',fontSize:'30px', alignSelf:'center',color:'#00AC8D'}} variant="h6" >Interaction </Typography>
                    <div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>Protein 1</Typography>
                                <Typography>{dataLinkInfo.protein1}</Typography>
                            </div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>Protein 2</Typography>
                                <Typography >{dataLinkInfo.protein2}</Typography>
                            </div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>DB Source</Typography>
                                <Typography >{dataLinkInfo.dbSource}</Typography>
                            </div>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <Typography variant='h6'>Weigth</Typography>
                                <Typography >{dataLinkInfo.weigth}</Typography>
                            </div>
                       </div>
                    </div>

                </Grid>
            </Grid>
        </Fragment>

    );
}


export default InteractionsNetworkPage;