import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    cellHeader :{
        backgroundColor:'#C4C4C4',
        fontSize:'20px',
    },
    button:{
        padding:'10px 25px',
        backgroundColor:'#70B46F',
        borderRadius:'25px',
        '&:hover':{
          backgroundColor:'rgb(43, 130, 207)'
        },
        marginLeft:'30px',
        marginBottom:'20px'
    },
    buttonRow:{
        padding:'10px 25px',
        backgroundColor:'#59A9F2',
        borderRadius:'25px',
        '&:hover':{
          backgroundColor:'rgb(43, 130, 207)'
        },
        marginLeft:'30px',
        marginBottom:'20px',
        boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
    },
    cardInformation:{
        //backgroundColor : '#F0F0F0',
        borderStyle : 'solid',
        marginBottom: '5px',
        marginLeft:'10px',
        marginTop:'10px',
        padding: '10px',
        display : 'flex',
        flexDirection : 'column'
        
    },
    myConfigGraph : {
        nodeHighlightBehavior: true,
        node: {
          color: "lightgreen",
          size: 120,
          highlightStrokeColor: "blue",
        },
        link: {
          highlightColor: "lightblue",
        },
    },
    resize: {
        fontSize: 50
    },
}));