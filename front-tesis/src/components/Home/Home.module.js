import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 280;

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    background: '#1aa193',
    color: '#000',
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    background: '#1aa193'
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  titleToolbar: {
    color: '#000',
    background: '#e2e2e2',
    paddingLeft: 20,
    paddingTop: 16,
    paddingBottom: 16,
  },
  userSectionToolbar: {
    marginTop: 20,
    marginBottom: 20,
  },
  userSectionToolbarImg: {
    textAlign: "center",
    color: '#000',
  },
  userSectionToolbarText: {
    textAlign: "center",
    color: '#000',
  },
  logoSection: {
    paddingTop: 7.5,
    paddingBottom: 7.5,
    paddingLeft: 15,
    backgroundColor: '#1aa193',
  }
}));