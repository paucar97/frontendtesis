import React,{useEffect,useContext} from "react";
import { useTheme } from "@material-ui/core/styles";
import {
  Drawer,
  AppBar,
  Toolbar,
  CssBaseline,
  Typography,
  IconButton,
  Hidden,
  Grid,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import Logo from '../../assets/img/logo.png';
import { routes } from "./../../routes";
import Sidebar from "./../Sidebar/Sidebar";
import { Route, Switch, Redirect } from "react-router-dom";
import { useStyles } from "./Home.module";
import { MainContext } from '../../context/mainContext';
export default function Home(props) {
  const context=useContext(MainContext);
  const classes = useStyles();
  const { window } = props;
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
    
  };

  const firstPath = routes ? routes[3].path:"";
  

  const drawer = (
    <div>
      
      <Grid item md={12} className={classes.userSectionToolbar}>
        <Grid className={classes.userSectionToolbarImg} item md={12}>
          <img style={{height:'100%',width:'90%',backgroundColor:'#29a79b'}} src={Logo} alt="Hippie"/>
        </Grid>
        <Grid item md={12} className={classes.userSectionToolbarText}>
          <Typography style={{textAlign:'middle',fontSize:'30px'}} variant="h6">HIPPIE</Typography>
        </Grid>
        <Grid item md={12} className={classes.userSectionToolbarText}>
            <div style={{display:'flex',alignItems:'center',backgroundColor:'#29a79b',padding:'10px'}}>
              <Typography style={{fontSize:'20px',textAlign:'middle',alignSelf:'center'}} variant="subtitle1">Human Integrated Protein-Protein Interaction rEference</Typography>
            </div>
        </Grid>
      </Grid>
      <Sidebar {...props}/>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerToggle}
            edge="start"
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            {context.menuInformation ? context.menuInformation:'HIPPIE'} 

          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>

      {/* CONTENT */}
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          {routes.map((r) => (
            <Route
              path={r.path}
              component={r.component}
              key={r.name}
              
            />
          ))}
          <Redirect from="/home" to={firstPath} />
        </Switch> 
      </main>
    </div>
  );
}
