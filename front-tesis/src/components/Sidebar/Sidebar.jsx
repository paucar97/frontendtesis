import React,{useContext} from 'react';
import { ListItem, ListItemIcon, ListItemText, List, Divider } from '@material-ui/core';
import {routes} from './../../routes';
import {useStyles} from './Sidebar.module';
import { NavLink } from 'react-router-dom';
import { MainContext } from '../../context/mainContext';

const Sidebar = () => {
  const context=useContext(MainContext);
  const classes = useStyles();
  
  const renderMenuHeader = (name) =>{
    const nameMenu =  JSON.parse(localStorage.getItem(name));
  
    context.setMenuInformation(name);
    console.log(name)
  }
  return (
      <div>
        <List>
            {/* Aca puedes hacer una iteración en las rutas que se mostrarán en el sidebar */}
            {routes.filter(route=>route.showInSidebar===true).map((r)=>(
              <NavLink
                to={r.path}
                key={r.name}
                className={classes.item}
                activeClassName={classes.activeStyle}
                >
                <ListItem
                  button
                  key={r.name}
                  className={classes.listItem}
                  onClick = {() =>{renderMenuHeader(r.name)}}
                  >
                    <ListItemIcon className={classes.listIcon}>
                      <r.icon style={{ fontSize: 30 }}/>
                    </ListItemIcon>
                    <ListItemText primary={r.name}/>
                  </ListItem>
              </NavLink>
            ))}
        </List>
      </div>
  );
};

export default Sidebar;