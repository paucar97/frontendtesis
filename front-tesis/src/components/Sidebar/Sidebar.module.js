import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    item: {
        position: "relative",
        textDecoration: "none",
        color: '#000',
        display: "block",
    },
    listIcon: {
        color: '#000',
    },
    listItem: {
        width: "auto",
        position: "relative",
        padding: "10px 15px",
        backgroundColor: "#29a79b",
        marginBottom:'15px',
        color: '#000',
        "&:hover": {
            color: '#000',
            background: '#3c7969',
        },
    },
    activeStyle: {
        background:'#e2e2e2',
    },
    divider: {
        marginTop: 10,
        background: 'rbg(249,249,249)'
    }
}));