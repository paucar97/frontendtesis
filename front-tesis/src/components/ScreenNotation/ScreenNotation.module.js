import { makeStyles } from '@material-ui/core/styles';


export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    info:{
        backgroundColor:'#d9d9d9',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
    },
    cellHeader :{
        backgroundColor:'#C4C4C4',
        fontSize:'25px',
    },
    infoScreenNotation:{
        backgroundColor:'#F6F6F6',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
        marginTop:'25px'
    },
    buttonSubmit:{
        padding:'10px 25px',
        backgroundColor:'#53aaf7',
        borderRadius:'25px',
        '&:hover':{
          backgroundColor:'rgb(43, 130, 207)'
        },
        marginLeft : '750px',
        marginTop:'10px',
        alignItems: "center",
        justifyContent: "center",
    },
}));