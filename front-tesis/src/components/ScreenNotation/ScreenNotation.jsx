import React, {Fragment} from 'react';
import { useStyles } from "./ScreenNotation.module";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import UploadFiles from "../DropzoneFile/UploadFilesComp";
import { Button } from '@material-ui/core';
 
const ScreenNotation = () => {
  const nameActualMenu = {name: "Screen Notation"};
  localStorage.setItem('nameActualMenu', JSON.stringify(nameActualMenu));
  const fileProtein = [];
  const classes = useStyles();

  const [identifierProtein, setidentifierProtein] = React.useState(1);
  const [separatorColumns, setSeparatorColoumns] = React.useState(1);
  const [numberInteractor2, setNumberInteractor2] = React.useState(1);
  const [numberInteractor1, setNumberInteractor1] = React.useState(1);
  const handleChangeIdentifierProtein = (event) => {
    setidentifierProtein(event.target.value);
  };

  const handleSeparatorColumns = (event) =>{
    setSeparatorColoumns(event.target.value);
  };

  const handleNumberInteractor2 = (event)=>{
    setNumberInteractor2(event.target.value);
  };
  const handleNumberInteractor1 = (event)=>{
    setNumberInteractor1(event.target.value);
  };

  return (
    <Fragment>
      <div className={classes.info}>
        In this section, you can get the score between two proteins present in HIPPIE database
      </div>
      <TableContainer component = {Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableBody>
            <TableRow>
              <TableCell className={classes.cellHeader}> Input</TableCell>
              <TableCell style={{"font-size":"30px"}}> File whit pair of protein to indicate interactions present in HIPPIE.</TableCell>
            </TableRow>
            <TableRow>
              <TableCell className={classes.cellHeader}> Output</TableCell>
              <TableCell style={{"font-size":"30px"}}> File whit extra column to indicate interactions present in HIPPIE.</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <div className={classes.infoScreenNotation}>
        <div style={{"margin-bottom":"40px"}} >Input configuration</div>
        <Grid container  >
          
            <div style={{'color':'red'}}>(*)</div>
          
          
          <div>Gene/protein identifier type:</div>
          
          
              <InputLabel id="identifierProtein-label" style={{"margin-left":"20px"}}></InputLabel>
              <Select
                labelId="identifierProtein-label"
                id="identifierProtein"
                value={identifierProtein}
                onChange={handleChangeIdentifierProtein}
                
              >
                <MenuItem value={1}>Entrez gene id</MenuItem>
                <MenuItem value={2}>Uniprot id</MenuItem>
              </Select>
          
        </Grid>
        <Grid container >
          <div style={{'color':'red'}}>(*)</div>
          <div>Separator between the columns: </div>
              <InputLabel id="separatorColumns-label" style={{"margin-left":"20px"}}></InputLabel>
                <Select
                  labelId="separatorColumns-label"
                  id="separatorColumns"
                  value={separatorColumns}
                  onChange={handleSeparatorColumns}
                  
                >
                <MenuItem value={1}>tab</MenuItem>
                <MenuItem value={2}>;</MenuItem>
                <MenuItem value={3}>,</MenuItem>
              </Select>
        </Grid>
        <Grid container>
          <div>Column number of interactor 2:</div>
          <InputLabel id="numberInteractor2-label" style={{"margin-left":"20px"}}></InputLabel>
          <Select
            labelId="numberInteractor2-label"
            id="numberInteractor2"
            value={numberInteractor2}
            onChange={handleNumberInteractor2}
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
          </Select>
        </Grid>
        <Grid container>
          <div>Column number of interactor 1:</div>
          <InputLabel id="numberInteractor1-label" style={{"margin-left":"20px"}}></InputLabel>
          <Select
            labelId="numberInteractor1-label"
            id="numberInteractor1"
            value={numberInteractor1}
            onChange={handleNumberInteractor1}
            
          >
            <MenuItem value={1}>1</MenuItem>
            <MenuItem value={2}>2</MenuItem>
            <MenuItem value={3}>3</MenuItem>
          </Select>
        </Grid>
        <Grid container>
          <div style={{'margin-top':'7px'}}>Ignore leading row (header):</div>
          <Checkbox
            defaultChecked
            color="primary"
            
            inputProps={{ 'aria-label': 'secondary checkbox' }}
          />
        </Grid>
      </div>
      <UploadFiles/>
      <Button className={classes.buttonSubmit}>Submit</Button>
    </Fragment>
    
  );
};

export default ScreenNotation;