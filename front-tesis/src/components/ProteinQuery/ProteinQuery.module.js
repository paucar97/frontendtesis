import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    info:{
        backgroundColor:'#d9d9d9',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
    },
    containerSearchProtein:{
        backgroundColor:'#FEFBFB',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
        display:"flex",
        alignItems:"center",
        justifyContent:"center"
    },
    resize: {
        fontSize: '20'
    },
    buttonSearch:{
        padding:'10px 25px',
        backgroundColor:'#53aaf7',
        borderRadius:'25px',
        '&:hover':{
          backgroundColor:'rgb(43, 130, 207)'
        },
        marginLeft : '20px',
     
    },
    buttonExample:{
        padding:'10px 25px',
        backgroundColor:'#BBD80C',
        borderRadius:'25px',
        '&:hover':{
          backgroundColor:'rgb(43, 130, 207)'
        },
        marginLeft : '20px',
     
    },
    layerInformation:{
        
        fontSize: 20,
        marginLeft : "22%"
    },
    iconInformation:{
        color:"#EDE067"
    }
}));