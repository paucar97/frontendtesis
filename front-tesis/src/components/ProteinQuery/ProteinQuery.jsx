import React,{Fragment,useContext} from 'react';
import { useStyles } from "./ProteinQuery.module";
import TextField from '@material-ui/core/TextField';
import { Button } from "@material-ui/core";
import { MainContext } from '../../context/mainContext';
import SearchIcon from '@material-ui/icons/Search';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import InfoIcon from '@material-ui/icons/Info';
import InfoTwoToneIcon from '@material-ui/icons/InfoTwoTone';
import LoaderProtein from "../LoaderProtein/LoaderProtein"; 
import {getIdentifiersProtein} from '../../api/index';
const ProteinQuery = (props) => {
  const nameActualMenu = {name: "Protein Query"};
  localStorage.setItem('nameActualMenu', JSON.stringify(nameActualMenu));
  const classes = useStyles();
  const context=useContext(MainContext);
  const [valueLabel,setValueLabel] = React.useState("");
  const [proteinIdentifiersAux,setProteinIdentifiersAux] = React.useState(false);
  const searchProtein = async () =>{
    await setProteinIdentifiersAux(true);
    const dataApi = await getIdentifiersProtein(valueLabel);
    let proteinIdentifiers_aux = {...context.proteinIdentifiers};
    proteinIdentifiers_aux = dataApi.data; 
    await context.setProteinIdentifiers(proteinIdentifiers_aux);
    await localStorage.setItem('proteinIdentify', JSON.stringify(proteinIdentifiers_aux));
    //console.log(proteinIdentifiers_aux);
    props.history.push({pathname:"/home/proteinInteraction"});

    
  };
  const handleValueLabel = async (event) =>{
    setValueLabel(event.target.value);
  };
  return (
      <Fragment>
        <div className = {classes.info}>
          In this section you can search interactions about one protein in HIPPIE database
        </div>
        <div className = {classes.containerSearchProtein}>
            <form>
              <TextField onChange={(e) => {handleValueLabel(e)}} value={valueLabel} id="standard-basic" label="Search" InputProps={{ classes: { input: classes.resize } }}/>
            </form>
            <Button  className = {classes.buttonSearch} onClick={searchProtein} startIcon={<SearchIcon/>} size = "large" > Search</Button>
            <Button  className = {classes.buttonExample} startIcon={<HelpOutlineIcon/>} size = "large"> Example</Button>
        </div>
        <div className={classes.layerInformation}>
          <InfoIcon className= {classes.iconInformation} fontSize="large"/>
          (e.g. HD_HUMAN, P42858, HTT or 3064)
        </div>
        { !proteinIdentifiersAux?
            <div></div>
          :
          <LoaderProtein/>
        }
      </Fragment>

  );
};

export default ProteinQuery;