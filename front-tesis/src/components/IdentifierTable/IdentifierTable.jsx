import React,{Fragment ,useContext,useEffect} from 'react';
import { useStyles } from "./IdentifierTableComp.module";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { MainContext } from '../../context/mainContext';
const IdentifierTable = (props) => {
    const classes = useStyles();
    const context=useContext(MainContext);
    useEffect(() => {
    }, [context.proteinIdentifiers]);
    const {proteinIdentifierObj} = props;
    return (
        <Fragment>
            <TableContainer component = {Paper}>
                <Table className={classes.table} aria-label="simple table">
                <TableBody>
                    <TableRow>
                    <TableCell className={classes.cellHeader}>Entrez ID </TableCell>
                    <TableCell style={{"font-size":"20px"}}>{proteinIdentifierObj.id_entrez} </TableCell>
                    <TableCell className={classes.cellHeader}> Uniprot ID</TableCell>
                    <TableCell style={{"font-size":"20px"}}> {proteinIdentifierObj.id_uniprot}</TableCell>
                    <TableCell className={classes.cellHeader}> Gene symbol</TableCell>
                    <TableCell style={{"font-size":"20px"}}> {proteinIdentifierObj.gene_symbol}</TableCell>
                    </TableRow>
                </TableBody>
                </Table>
            </TableContainer>
        </Fragment>
    );

};

export default IdentifierTable;