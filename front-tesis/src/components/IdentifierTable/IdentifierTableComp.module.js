import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    cellHeader :{
        backgroundColor:'#C4C4C4',
        fontSize:'20px',
    },
}));