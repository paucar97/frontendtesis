import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  files: {
    marginTop: 20,
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));
