import React, { createContext,useContext } from 'react';
import {useStyles} from './Dropzone.module';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Paper } from '@material-ui/core';
import { MainContext } from '../../context/mainContext';
const Dropzone = (props) => {
    const classes = useStyles();
    const fileInputRef = React.createRef();
    const {disabled} = props;
    const context=useContext(MainContext);
    const openFileDialog = () => {
        if(disabled) return;
        fileInputRef.current.click();
    }

    const fileListToArray = (list) => {
        const array = [];
        for(var i = 0; i<list.length; i++) {
            array.push(list.item(i));
        }
        return array;
    }

    const onFilesAdded = (e) => {
        console.log(e);
        if(disabled) return;
        const files = e.target.files;
        console.log(files);
        context.setFiles(files);
        
        
        if(props.onFilesAdded) {
            console.log("MARTIN VAN 2");
            const array = fileListToArray(files);
            props.onFilesAdded(array);
        }
    }

    const onDragOver = (e) => {
        console.log(e);
        e.preventDefault();
        if(disabled) return;
    }

    const onDrop = (e) => {
        e.preventDefault();
        if(disabled) return;
        const files = e.dataTransfer.files;
        if(props.onFilesAdded) {
            const array = fileListToArray(files);
            props.onFilesAdded(array);
        }
    }
    
    return (
        <Paper className={classes.dropzone}
            onDragOver={onDragOver}
            onDrop={onDrop}
            onClick={openFileDialog}
            style={{ cursor: disabled ? "default" : "pointer" }}
        >
            <CloudUploadIcon className={classes.icon}/>
            <input
                ref={fileInputRef}
                className={classes.fileInput}
                type="file"
                multiple
                onChange={onFilesAdded}
                />
            <span>Arrastre sus archivos aquí</span>
            <span>O haga clic para navegar</span>
        </Paper>
    );
};

export default Dropzone;