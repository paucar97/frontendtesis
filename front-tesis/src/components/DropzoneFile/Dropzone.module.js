import { makeStyles } from '@material-ui/core/styles';


export const useStyles = makeStyles(theme => ({
    dropzone: {
        paddingTop: 20,
        paddingBottom: 20,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        fontSize: 16,
        border: `2px dashed #1a202a`,
    },
    fileInput: {
        display: "none",
    },
    icon: {
        height: 64,
        width: 64,
        color: '#004178',
    },
    buttonUpload: {
        marginBottom: 10,
        cursor: "pointer",
        '&:hover': {
          opacity: 0.9,
        },
      },
    files: {
        marginTop: 20,
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
  }));