import React, {useState,useContext,useEffect} from 'react';
import Alert from '@material-ui/lab/Alert';
import {useStyles} from './UploadFilesComp.module';
import Dropzone from './Dropzone';
import { MainContext } from '../../context/mainContext';

const UploadFiles = () => {
    const context=useContext(MainContext)
    const classes = useStyles();
    const uploading = false;
    // const [uploading, setUploading] = useState(false);
    const successfullUploaded = false;
    // const [successfullUploaded, setSuccessfullUploaded] = useState(false);
    useEffect(() => {
    }, [context.files]);

    const onFilesAdded = (fileArray) => {
        let encontrado=false;
        if(context.files===[]){
            context.setFiles(prevFiles => context.files.concat(fileArray));
        }else{
            context.files.map((x)=>{
                if(x.name===fileArray[0].name){
                  
                    encontrado=true
                }
            })
            if(!encontrado){
                context.setFiles(prevFiles => context.files.concat(fileArray));
            }
        }
    }

    const formatBytes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';
    
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    
        const i = Math.floor(Math.log(bytes) / Math.log(k));
    
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    const eliminarArchivo =(data)=>{
        let aux=[...context.files]
        aux.map((x,index)=>{
            if(x.name===data){aux.splice(index,1)}
        })
        context.setFiles(aux)
    }

    return (
        <div className={classes.upload}>
            <Dropzone
                onFilesAdded={onFilesAdded}
                disabled={uploading || successfullUploaded}
            />
            <div className={classes.files}>
                {context.files.map(file => {
                    
                    return(
                        
                        <Alert key={file.name}
                            onClose={() => eliminarArchivo(file.name)}>{file.name} - {formatBytes(file.size)}</Alert>
                    )
                })}
            </div>
        </div>
    );
};

export default UploadFiles;
