import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 280;

export const useStyles = makeStyles((theme)=>({

    citeButton:{
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
          display: 'none',
        },
    },
    info:{
        backgroundColor:'#d9d9d9',
        padding:'10px',
        color:'#000',
        borderRadius:'10px',
        fontFamily:'Roboto',
        fontSize:'19px',
        marginBottom:'25px',
      },
    button:{
        padding:'10px 25px',
        backgroundColor:'#A9B6F9',
        borderRadius:'25px',
        '&:hover':{
          backgroundColor:'rgb(43, 130, 207)'
        },
        textAlign: "rigth",
        justifyContent: "rigth",
        marginLeft : "50%"
    },
    tableRow:{
        "&$hover:hover": {
            backgroundColor: "blue"
          },
         
    }
}))