import React, {Fragment}from 'react';
import MaterialTable from "material-table";
import { useStyles } from "./LandPage.module";
import { Button } from "@material-ui/core";

import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {Typography} from '@material-ui/core';
const LandPage = () => {
  const nameActualMenu = {name: "Home"};
  const classes = useStyles();
  localStorage.setItem('nameActualMenu', JSON.stringify(nameActualMenu));

  const listVersion =[
    {description: 'Feb 14, 2019  A new version of HIPPIE (v2.2) has been released today'},
    {description: 'Jul 18, 2017  The update to HIPPIE v2.1 contains 52,000 new interactions'},
    {description: 'Nov 3, 2016  A new paper is out describing the new functionality and data of HIPPIE v2.0'},
    {description: 'Jun 24, 2016  HIPPIE v2.0 has been released including new data and analyses options'},
  ]

  return (
    <Fragment>
      <div className={classes.info}>
        <Typography style={{textAlign:'middle',fontSize:'80px'}} variant="h5">
          Welcome 
        </Typography>
        <Typography style={{textAlign:'middle',fontSize:'30px'}} variant="h4">
          Web tool to generate reliable and meaningful human protein-protein interaction networks.
        </Typography>
      </div>
      <div className={classes.info}> 
        News
        <br/>
        <br/>
        <TableContainer component = {Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableBody>
              {listVersion.map((row) => (
                <TableRow key={row.description} className={classes.tableRow}>
                  <TableCell component="th" scope="row">
                    {row.description}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      <div className={classes.info}>
        Cite <br/>
        Please cite HIPPIE within any publication that makes use of this resource. Thanks!
        <Button className={classes.button}  >Cite!</Button>
      </div>
    </Fragment>
  );
};

export default LandPage;