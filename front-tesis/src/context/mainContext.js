import React, { useState, createContext } from "react";

export const MainContext = createContext();

export const MainProvider = (props) => {
    const [files, setFiles] = useState([]);
    const [uniprot_id,setUniprot_id] = useState(null);
    const [entrez_id,setEntrez_id] = useState(null);
    const [gene_symbol,setGene_symbol] = useState(null);
    const [proteinIdentifiers,setProteinIdentifiers] = useState(null);
    const [dataListInteractions, setDataListInteractions] = useState(null);
    const [dataGraphProteinsInteractionsIndirect , setDataGraphProteinsInteractionsIndirect] = useState(null);
    const [menuInformation, setMenuInformation] = useState(null);
    const obj = {
        files,
        setFiles,
        uniprot_id,
        setUniprot_id,
        entrez_id,
        setEntrez_id,
        gene_symbol,
        setGene_symbol,
        proteinIdentifiers,
        setProteinIdentifiers,
        menuInformation,
        setMenuInformation,
        dataListInteractions,
        setDataListInteractions,
        dataGraphProteinsInteractionsIndirect,
        setDataGraphProteinsInteractionsIndirect
    }

  return (
    <MainContext.Provider value={obj}>{props.children}</MainContext.Provider>
  );
};
