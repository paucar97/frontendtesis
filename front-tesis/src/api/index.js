import axios from "axios";

const getAPIBack = () => {
    var apiBack;
    apiBack = process.env.REACT_APP_API_BACK;
    return apiBack;
  };

  const api = axios.create({
    baseURL: getAPIBack(),
    headers: {
      "X-Requested-With": "XMLHttpRequest",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "POST,GET",
      Accept: "*/*",
    },
  });

  //Aca pones todas las llamadas a los servicios:
  /* export const func = async () =>{
      const response = await api({
          method:"post",
          url:"/api/url"
      });
      aca pones los casos de success y error:
      if(response.status >= 200 && response.status < 300) return response.data;
      else return null;
  } */

  export const getProteins = async() =>{
    
    const response = await api ({
      method : "get",
      url : "/api/protein"
    });
    //console.log(response.data);
    return response.data;
  }

  export const getInteractionsOneProteins = async(idProtein) =>{
    
    const response = await api ({
      method : "get",
      url : "/api/interactionsProtein/" + idProtein.toString()
    });
    //console.log(response.data);
    return response.data;
  }
  export const getSequenceInfo = async(entrezId) =>{
    const response = await api ({
      method : "get",
      url : "/api/get_sequence_info/" + entrezId.toString()
    });
    //console.log(response.data);
    return response.data;
  }

  export const getIdentifiersProtein = async(unknowIdentifierProtein) =>{
    const response = await api ({
      method : "get",
      url : "/api/get_identifiers/" + unknowIdentifierProtein.toString()
    });
    //console.log(response.data);
    return response.data;
  }
  export const getIteractionsProteins = async(listUnknownIdenfierProtein, dFactor) =>{
    const response = await api ({
      method : "get",
      url : "/api/show_interactions/" + listUnknownIdenfierProtein.toString() + "/" + dFactor.toString()
    });
    //console.log(response.data);
    return response.data;
  }

  export const getIteractionsProteinsIndirect = async(listUnknownIdenfierProtein, dFactor) =>{
    const response = await api ({
      method : "get",
      url : "/api/indirect_interactions/" + listUnknownIdenfierProtein.toString() + "/" + dFactor.toString()
    });
    //console.log(response.data);
    return response.data;
  }