import TimelineIcon from '@material-ui/icons/Timeline';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import GetAppIcon from '@material-ui/icons/GetApp';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import BrowseProtein from './components/BrowseProtein/BrowseProtein';
import ProteinQuery from './components/ProteinQuery/ProteinQuery';
import NetworkQuery from './components/NetworkQuery/NetworkQuery';
import LandPage from './components/LandPage/LandPage';
import ScreenNotation from './components/ScreenNotation/ScreenNotation';
import DatabaseDownload from './components/DatabaseDownload/DatabaseDownload';
import InteractionsProteins from './components/InteractionsProteins/InteractionsProteins';
import InteractionsNetworkPage from './components/InteractionsNetworkPage/InteractionsNetworkPage';
import InteractionsNetworkPageIndirect from './components/InteractionsNetworkPageIndirect/InteractionsNetworkPageIndirect';
//Aca pon todas las rutas para el router
export const routes =[
    {
        path: "/home/ProteinQuery",
        name: "Protein Query",
        icon: TimelineIcon,
        component: ProteinQuery,
        showInSidebar: true,
      },
      {
        path: "/home/NetworkQuery",
        name: "Network Query",
        icon: TimelineIcon,
        component: NetworkQuery,
        showInSidebar: true,
      },
      {
        path: "/home/LandPage",
        name: "Home",
        icon: HomeIcon,
        component: LandPage,
        showInSidebar: true,
      },
      {
        path: "/home/BrowseProtein",
        name: "Browse protein",
        icon: SearchIcon,
        component: BrowseProtein,
        showInSidebar: true,
      },
      {
        path: "/home/ScreenNotation",
        name: "Screen notation",
        icon: NoteAddIcon,
        component: ScreenNotation,
        showInSidebar: false,
      },{
        path: "/home/DatabaseDownload",
        name: "Database Download",
        icon: GetAppIcon,
        component: DatabaseDownload,
        showInSidebar: false,
      },        
      {
        path: "/home/login",
        name: "Login/Register",
        icon: PermIdentityIcon,
        component: BrowseProtein,
        showInSidebar: false,
      },
      {
        path: "/home/proteinInteraction",
        name: "Protein Interactor",
        icon: PermIdentityIcon,
        component: InteractionsProteins,
        showInSidebar: false,
      },
      {
        path: "/home/InteractionsNetwork",
        name: "Interaction Network",
        icon: PermIdentityIcon,
        component: InteractionsNetworkPage,
        showInSidebar: false,
      },
      {
        path: "/home/InteractionsNetworkProteins",
        name: "Interaction Network Proteins",
        icon: PermIdentityIcon,
        component: InteractionsNetworkPageIndirect,
        showInSidebar: false,
      }
]